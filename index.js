let express = require("express");
const PORT = 3000;

let app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req,res) => res.send(`Welcome to the home page`));


let array =[];
app.post("/signup", (req,res) => {
	// console.log(req.body);
	 if (req.body.username !== "" && req.body.password !== ""){
	 res.send(`User ${req.body.username} successfully registered!`);
	 array.push(req.body);
	 console.log(array);
	 } else {
	 	res.send(`Please input BOTH username and password.`);
	 }

} );

app.get("/users", (req,res) => res.send(JSON.stringify(array)));


app.listen(PORT, () => {
	console.log(`Server is now connected to port ${PORT}.`)

});